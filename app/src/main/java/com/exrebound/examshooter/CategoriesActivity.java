package com.exrebound.examshooter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooter.models.Category;
import com.exrebound.examshooter.models.ErrorMessage;
import com.exrebound.examshooter.utils.Parser;
import com.exrebound.examshooter.utils.Utils;

import java.util.ArrayList;

public class CategoriesActivity extends AppCompatActivity {

    public static final String EXTRA_CATEGORY_ID = ".category_id";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private ListView categoriesListView;
    private TextView emptyTextView;
    private LinearLayout containerLinearLayout;
    private ProgressBar mProgressView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private RequestQueue queue;
    private ArrayAdapter<Category> adapter;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prepareUI();
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        categoriesListView = (ListView) findViewById(R.id.LVCategories);
        emptyTextView = (TextView) findViewById(R.id.empty);
        categoriesListView .setEmptyView(emptyTextView);
        getCategories();
        categoriesListView .setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category c = (Category) categoriesListView.getItemAtPosition(position);
                Intent i = new Intent(CategoriesActivity.this, PostsActivity.class);
                i.putExtra(EXTRA_CATEGORY_ID, c.getId());
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Save User Login Status to prefs
        boolean isLogin = getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean(LoginActivity.EXTRA_IS_LOGIN, false);
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        //If login, show logout
        if(isLogin) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }
        //If logged out, show login
        else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.MILogOut) {
            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();
        }
        if(id == R.id.MILogIn){
            //Go to login activity
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();

            startActivity(intent);
            finish();
        }        return super.onOptionsItemSelected(item);
    }

    public void getCategories() {
        String URL = "http://everythinghere.in/app/index.php/category";
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ArrayList<Category> categories = Parser.getCategories(s);
                            if (categories != null && categories.size() > 0) {
                                adapter = new ArrayAdapter<>(CategoriesActivity.this, android.R.layout.simple_list_item_1, categories);
                                categoriesListView.setAdapter(adapter);
                            } else {
                                ErrorMessage e = Parser.getError(s);
                                if (e != null)
                                    Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            queue = Volley.newRequestQueue(CategoriesActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
