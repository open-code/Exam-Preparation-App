package com.exrebound.examshooter;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooter.utils.Utils;

import info.androidhive.listviewfeed.data.Post;

public class PostDetailActivity extends AppCompatActivity {

    private static final String IMAGE_URL = "http://everythinghere.in/app/images/";
    private RequestQueue queue;
    private ProgressBar mProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int id = getIntent().getIntExtra(PostsActivity.EXTRA_ID, 0);
        String title = getIntent().getStringExtra(PostsActivity.EXTRA_TITLE);
        String image = getIntent().getStringExtra(PostsActivity.EXTRA_IMAGE);
        String date = getIntent().getStringExtra(PostsActivity.EXTRA_DATE);
        String content = getIntent().getStringExtra(PostsActivity.EXTRA_CONTENT);

        Post p = new Post(id, title, image, date, content);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prepareUI(p);
    }

    private void prepareUI(Post item) {
        mProgressView = (ProgressBar) findViewById(R.id.PB);
        mProgressView.setVisibility(View.GONE);
        TextView name = (TextView) findViewById(R.id.name);
        TextView timestamp = (TextView) findViewById(R.id.timestamp);
        TextView statusMsg = (TextView) findViewById(R.id.txtStatusMsg);
        ImageView feedImageView = (ImageView) findViewById(R.id.feedImage1);

        name.setText(item.getTitle());

        timestamp.setText(item.getDate());

        // Check for empty status message
        if (!TextUtils.isEmpty(item.getContent())) {
            statusMsg.setText(item.getContent());
            statusMsg.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            statusMsg.setVisibility(View.GONE);
        }

        // Feed image
        if (item.getImage() != null && item.getImage().length() > 0 && !item.getImage().equals("-")) {
            getImage(IMAGE_URL + item.getImage(), feedImageView);
        } else {
            feedImageView.setVisibility(View.GONE);
        }
    }

    private void getImage(String url, final ImageView feedImageView) {
        url = url.replace(" ", "%20");
        if (Utils.isNetworkOnline(PostDetailActivity.this)) {
            showProgress(true);
            ImageRequest request = new ImageRequest(url, new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap bitmap) {
                    showProgress(false);
                    feedImageView.setImageBitmap(bitmap);
                    feedImageView.setVisibility(View.VISIBLE);
                }
            }, 0, 0, null, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    showProgress(false);
                    Utils.showAlertDialog(PostDetailActivity.this, "Error", "Error Occurred while Downloading Image..!");
                }
            });
            if (queue == null)
                queue = Volley.newRequestQueue(PostDetailActivity.this);
            queue.add(request);
        } else {
            Utils.showAlertDialog(PostDetailActivity.this, "Error", "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

}
