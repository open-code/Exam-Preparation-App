package com.exrebound.examshooter;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooter.models.Question;
import com.exrebound.examshooter.utils.NonSwipeableViewPager;
import com.exrebound.examshooter.utils.Parser;
import com.exrebound.examshooter.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class QuizActivity extends AppCompatActivity {

    private static final String TAG = ".quiz_activity";
    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private NonSwipeableViewPager questionsViewPager;
    private LinearLayout containerLinearLayout;
    private ProgressBar mProgressView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private RequestQueue queue;
    private QuestionsPagerAdapter adapter;
    private List<Fragment> fragments;
    private CountDownTimer timer;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    private int quizID;
    private int quizDuration;
    private ArrayList<Question> questions;
    private ArrayList<String> answers;
    private boolean hasSubmitted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        quizID = getIntent().getIntExtra(MainActivity.EXTRA_QUIZ_ID, 0);
        quizDuration = getIntent().getIntExtra(MainActivity.EXTRA_QUIZ_DURATION,0);
        String quizName = getIntent().getStringExtra(MainActivity.EXTRA_QUIZ_NAME) == null ? "" : getIntent().getStringExtra(MainActivity.EXTRA_QUIZ_NAME);
        setTitle(quizName);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prepareUI();
    }

    public int getQuizID() {
        return quizID;
    }

    public RequestQueue getQueue() {
        if(queue == null){
            queue = Volley.newRequestQueue(QuizActivity.this);
        }
        return queue;
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        questionsViewPager = (NonSwipeableViewPager) findViewById(R.id.PVQuestions);
        if (quizID != 0) getQuestions(quizID);
    }

    public void getQuestions(int quizID) {
        String URL = "http://everythinghere.in/app/index.php/question/" + quizID;
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            questions = Parser.getQuestions(s);
                            if(questions == null || questions.isEmpty()) Utils.showAlertDialog(QuizActivity.this,"Empty","No Questions Found In Selected Quiz");
                            else startQuiz(questions);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            queue = Volley.newRequestQueue(QuizActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    private void startQuiz(ArrayList<Question> questions) {
        //If quiz's duration is 0, assume it to have no time restrictions
        if(quizDuration > 0){
            //Start Timer
            timer = new CountDownTimer(quizDuration*60000, 1000) {
                @Override public void onTick(long l) { ((QuestionFragment) adapter.getItem(questionsViewPager.getCurrentItem())).updateTime(l); }
                @Override public void onFinish() { FinishQuiz(); }
            };
            timer.start();
        }
        //Prepare Layout (PageView)
        fragments = getFragments(questions);
        //Add Contents
        adapter = new QuestionsPagerAdapter(getSupportFragmentManager(), fragments);
        questionsViewPager.setAdapter(adapter);
        answers = new ArrayList<>(questions.size());
    }

    public void StopTimer(){
        if(timer != null) timer.cancel();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        StopTimer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                StopTimer();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void FinishQuiz() {
        QuestionFragment fragment = (QuestionFragment) adapter.getItem(questionsViewPager.getCurrentItem());
        fragment.FinishQuiz();
    }

    private List<Fragment> getFragments(ArrayList<Question> questions) {
        List<Fragment> fragments = new ArrayList<>();
        for (Question q : questions)
            fragments.add(QuestionFragment.newInstance(q));
        return fragments;
    }

    public ArrayList<Question> getQuestions(){
        return questions;
    }

    public void setCurrentItem(int item, boolean smoothScroll) {
        questionsViewPager.setCurrentItem(item, smoothScroll);
    }

    public boolean hasSubmitted() {
        return hasSubmitted;
    }

    public void setHasSubmitted(boolean hasSubmitted) {
        this.hasSubmitted = hasSubmitted;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {
        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void addAnswerAtPosition(String answer, int currentIndex) {
        if(currentIndex >= answers.size())
            answers.add(currentIndex,answer);
        else
            answers.set(currentIndex, answer);
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }


    private class QuestionsPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public QuestionsPagerAdapter(FragmentManager supportFragmentManager, List<Fragment> fragments) {
            super(supportFragmentManager);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }
}
