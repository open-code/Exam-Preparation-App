package com.exrebound.examshooter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooter.models.Category;
import com.exrebound.examshooter.models.ErrorMessage;
import com.exrebound.examshooter.utils.Parser;
import com.exrebound.examshooter.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.listviewfeed.data.Post;

public class Menu2Activity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_LEADERBOARD = "com.exrebound.examshooter.leaderboard";
    private static final String TAG = ".menu_activity";

    private static final String EXTRA_USER_ID = "user_id";
    public static final String EXTRA_CATEGORY_ID = ".category_id";

    public static final String EXTRA_CONTENT = ".content";
    public static final String EXTRA_TITLE = ".title";
    public static final String EXTRA_DATE = ".date";
    public static final String EXTRA_ID = ".id";
    public static final String EXTRA_IMAGE = ".image";
    public static final String EXTRA_IMAGE_STRING = ".image_string";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private ListView categoriesListView, postsListView;
    private TextView emptyTextView;
    private LinearLayout containerLinearLayout, quizzesLinearLayout, historyLinearLayout, leaderboardLinearLayout, logoutLinearLayout;
    private ProgressBar mProgressView;
    private FeedListAdapter feedAdapter;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private RequestQueue queue;
    private ArrayAdapter<Category> adapter;
    private Menu menu;
    private TextView item;
    private NavigationView navigationView;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    private int userID;
    private boolean isLogin;
    private ArrayList<Post> posts;
    private ArrayList<Category> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Posts");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        userID = getIntent().getIntExtra(LoginActivity.EXTRA_USER_ID, 0);
        //Save User ID to prefs
        getSharedPreferences("user_id", Context.MODE_PRIVATE).edit().putInt(EXTRA_USER_ID, userID).commit();

        prepareUI();

        //Save User Login Status to prefs
        isLogin = getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean(LoginActivity.EXTRA_IS_LOGIN, false);
        if (!isLogin) item.setText("Log In");
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        item = (TextView) findViewById(R.id.TVLogout);
        categoriesListView = (ListView) findViewById(R.id.LVCategories);
        emptyTextView = (TextView) findViewById(R.id.empty);
        categoriesListView.setEmptyView(emptyTextView);
        getCategories();
        categoriesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (queue != null) queue.cancelAll(new RequestQueue.RequestFilter() {
                    @Override
                    public boolean apply(Request<?> request) {
                        return true;
                    }
                });
                Category c = (Category) categoriesListView.getItemAtPosition(position);
                getPosts(c.getId());
                setTitle(c.getName());

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        postsListView = (ListView) findViewById(R.id.LVPosts);
        postsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (queue != null) queue.cancelAll(new RequestQueue.RequestFilter() {
                    @Override
                    public boolean apply(Request<?> request) {
                        return true;
                    }
                });
                Log.e(TAG,"Clicked!");
                Post p = (Post) postsListView.getItemAtPosition(position);
                Intent i = new Intent(Menu2Activity.this, PostDetailActivity.class);
                i.putExtra(EXTRA_CONTENT, p.getContent());
                i.putExtra(EXTRA_TITLE, p.getTitle());
                i.putExtra(EXTRA_DATE, p.getDate());
                i.putExtra(EXTRA_ID, p.getId());
                i.putExtra(EXTRA_IMAGE, p.getImage());
                i.putExtra(EXTRA_IMAGE_STRING, p.getImageString());
                startActivity(i);
            }
        });
        quizzesLinearLayout = (LinearLayout) findViewById(R.id.LLQuizzes);
        historyLinearLayout = (LinearLayout) findViewById(R.id.LLHistory);
        leaderboardLinearLayout = (LinearLayout) findViewById(R.id.LLLeaderboard);
        logoutLinearLayout = (LinearLayout) findViewById(R.id.LLLogOut);
        quizzesLinearLayout.setOnClickListener(this);
        historyLinearLayout.setOnClickListener(this);
        leaderboardLinearLayout.setOnClickListener(this);
        logoutLinearLayout.setOnClickListener(this);
        postsListView.setEmptyView(emptyTextView);
    }


    public void getPosts(int categoryID) {
        String URL = "http://everythinghere.in/app/index.php/post/category/" + categoryID;
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            posts = Parser.getPosts(s);
                            if (posts != null && posts.size() > 0) {
                                feedAdapter = new FeedListAdapter(Menu2Activity.this, posts);
                                postsListView.setAdapter(feedAdapter);
                            } else {
                                ErrorMessage e = Parser.getArrayError(s);
                                if (e != null)
                                    Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            if (queue == null) queue = Volley.newRequestQueue(Menu2Activity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        //Save User Login Status to prefs
        boolean isLogin = getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean(LoginActivity.EXTRA_IS_LOGIN, false);
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        //If login, show logout
        if (isLogin) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }
        //If logged out, show login
        else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.MILogOut) {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
            isLogin = false;
            navigationView.getMenu().getItem(3).setTitle("Log In");
            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();
        }
        if (id == R.id.MILogIn) {
            //Go to login activity
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();

            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void getCategories() {
        String URL = "http://everythinghere.in/app/index.php/category";
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            categories = Parser.getCategories(s);
                            if (categories != null && categories.size() > 0) {
                                adapter = new ArrayAdapter<>(Menu2Activity.this, android.R.layout.simple_list_item_1, categories);
                                categoriesListView.setAdapter(adapter);
                                //Get selected index
                                int index = categoriesListView.getSelectedItemPosition();
                                //If no item selected then first items id else use selected index
                                int id = (index >= 0) ? categories.get(index).getId() : categories.get(0).getId();
                                getPosts(id);
                            } else {
                                ErrorMessage e = Parser.getError(s);
                                if (e != null)
                                    Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            queue = Volley.newRequestQueue(Menu2Activity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        // Handle navigation view item clicks here.
        int id = v.getId();
        if (id == R.id.LLQuizzes) {
            if (!isLogin) {
                Utils.showAlertDialogWithoutCancel(Menu2Activity.this, "Sign In", "Please Sign In To Access This Feature");
                return;
            }
            startActivity(new Intent(this, MainActivity.class));
        } else if (id == R.id.LLHistory) {
            if (!isLogin) {
                Utils.showAlertDialogWithoutCancel(Menu2Activity.this, "Sign In", "Please Sign In To Access This Feature");
                return;
            }
            Intent i = new Intent(this, RecordsActivity.class);
            i.putExtra(EXTRA_LEADERBOARD, false);
            int userID = getSharedPreferences("Id", Context.MODE_PRIVATE).getInt(EXTRA_USER_ID, 0);
            i.putExtra(EXTRA_ID, userID);
            startActivity(i);
        } else if (id == R.id.LLLeaderboard) {
            if (!isLogin) {
                Utils.showAlertDialogWithoutCancel(Menu2Activity.this, "Sign In", "Please Sign In To Access This Feature");
                return;
            }

            Intent i = new Intent(this, RecordsActivity.class);
            i.putExtra(EXTRA_LEADERBOARD, true);
            startActivity(i);
        } else if (id == R.id.LLLogOut) {
            if (item.getText().equals("Log Out")) {
                item.setText("Log In");
                menu.getItem(0).setVisible(false);
                menu.getItem(1).setVisible(true);
                //Save User Login Status to prefs
                getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();
                isLogin = false;
            } else if (item.getText().equals("Log In")) {
                item.setText("Log Out");
                //Go to login activity
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public class FeedListAdapter extends BaseAdapter {
        private static final String IMAGE_URL = "http://everythinghere.in/app/images/";
        private Activity activity;
        private LayoutInflater inflater;
        private List<Post> posts;
        Bitmap image;

        public FeedListAdapter(Activity activity, List<Post> posts) {
            this.activity = activity;
            this.posts = posts;
        }

        @Override
        public int getCount() {
            return posts.size();
        }

        @Override
        public Object getItem(int location) {
            return posts.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (inflater == null)
                inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null)
                convertView = inflater.inflate(R.layout.feed_item2, null);

            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView timestamp = (TextView) convertView
                    .findViewById(R.id.timestamp);
            TextView statusMsg = (TextView) convertView
                    .findViewById(R.id.txtStatusMsg);
            ImageView feedImageView = (ImageView) convertView
                    .findViewById(R.id.feedImage1);

            Post item = posts.get(position);

            name.setText(item.getTitle());

            timestamp.setText(item.getDate());

            // Check for empty status message
            if (!TextUtils.isEmpty(item.getContent())) {
                Utils.makeTextViewResizable(statusMsg, 3, "Read More", false);
                statusMsg.setText(item.getContent());
                statusMsg.setVisibility(View.VISIBLE);
            } else {
                // status is empty, remove from view
                statusMsg.setVisibility(View.GONE);
            }


        /*
            // Feed image
            if (item.getImage() != null && item.getImage().length() > 0 && !item.getImage().equals("-")) {
                getImage(IMAGE_URL + item.getImage(), feedImageView);
            } else {
                feedImageView.setVisibility(View.GONE);
            }
        */
            return convertView;
        }

        private void getImage(String url, final ImageView feedImageView) {
            url = url.replace(" ", "%20");
            if (Utils.isNetworkOnline(Menu2Activity.this)) {
                showProgress(true);
                ImageRequest request = new ImageRequest(url, new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        showProgress(false);
                        feedImageView.setImageBitmap(bitmap);
                        feedImageView.setVisibility(View.VISIBLE);
                    }
                }, 0, 0, null, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        showProgress(false);
                        Utils.showAlertDialog(Menu2Activity.this, "Error", "Error Occurred while Downloading Image..!");
                    }
                });
                if (queue == null)
                    queue = Volley.newRequestQueue(Menu2Activity.this);
                queue.add(request);
            } else {
                Utils.showAlertDialog(Menu2Activity.this, "Error", "Network Not Found!");
            }
        }

    }
}
