package com.exrebound.examshooter.models;

/**
 * Created by Zuhaib on 12/23/2015.
 */
public class Record {

    int id, userId, quizID, questions, correct;
    double percentage;

    public Record(int id, int userId, int quizID, int questions, int correct, double percentage) {
        this.id = id;
        this.userId = userId;
        this.quizID = quizID;
        this.questions = questions;
        this.correct = correct;
        this.percentage = percentage;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public int getQuizID() {
        return quizID;
    }

    public int getQuestions() {
        return questions;
    }

    public int getCorrect() {
        return correct;
    }

    public double getPercentage() {
        return percentage;
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", userId=" + userId +
                ", quizID=" + quizID +
                ", questions=" + questions +
                ", correct=" + correct +
                ", percentage=" + percentage +
                '}';
    }
}
