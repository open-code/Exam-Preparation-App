package com.exrebound.examshooter.models;

/**
 * Created by hp sleek book on 11/17/2015.
 */
public class Quiz {

    private int success;
    private int id;
    private String title = "";
    private int duration;
    private int questions;
    private int endsIn;
    private String date_added;

    public Quiz(int id, String title, int duration, int questions, int endsIn, String date_added) {
        this.id = id;
        this.title = title;
        this.duration = duration;
        this.questions = questions;
        this.endsIn = endsIn;
        this.date_added = date_added;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getQuestions() {
        return questions;
    }

    public void setQuestions(int questions) {
        this.questions = questions;
    }

    public int getEndsIn() {
        return endsIn;
    }

    public void setEndsIn(int endsIn) {
        this.endsIn = endsIn;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
