package com.exrebound.examshooter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.exrebound.examshooter.models.ErrorMessage;
import com.exrebound.examshooter.models.Question;
import com.exrebound.examshooter.utils.Parser;
import com.exrebound.examshooter.utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hp sleek book on 12/16/2015.
 */
public class QuestionFragment extends Fragment {
    private static final String KEY_ID = "id";
    private static final String KEY_CONTENT = "content";
    private static final String KEY_ANSWER = "answer";
    private static final String KEY_OPTION1 = "op1";
    private static final String KEY_OPTION2 = "op2";
    private static final String KEY_OPTION3 = "op3";
    private static final String KEY_OPTION4 = "op4";
    private static final String KEY_OPTION5 = "op5";
    private static final String TAG = ".question_fragment";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    TextView questionTextView, answerTextView,timeTextView;
    RadioGroup optionsRadioGroup;
    RadioButton o1RadioButton, o2RadioButton, o3RadioButton, o4RadioButton, o5RadioButton;
    EditText answerEditText;
    Button nextButton, prevButton, submitButton;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private Question q;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    int currentIndex = -1;


    public static Fragment newInstance(Question q) {
        QuestionFragment fragment = new QuestionFragment();
        Bundle b = new Bundle();
        b.putInt(KEY_ID, q.getId());
        b.putString(KEY_CONTENT, q.getContent());
        b.putString(KEY_ANSWER, q.getAnswer());
        b.putString(KEY_OPTION1, q.getOption1());
        b.putString(KEY_OPTION2, q.getOption2());
        b.putString(KEY_OPTION3, q.getOption3());
        b.putString(KEY_OPTION4, q.getOption4());
        b.putString(KEY_OPTION5, q.getOption5());
        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle args = getArguments();
        q = new Question(args.getInt(KEY_ID), args.getString(KEY_CONTENT), args.getString(KEY_ANSWER), args.getString(KEY_OPTION1),
                args.getString(KEY_OPTION2), args.getString(KEY_OPTION3), args.getString(KEY_OPTION4), args.getString(KEY_OPTION5));

        View v = inflater.inflate(R.layout.fragment_question, container, false);

        //References
        questionTextView = (TextView) v.findViewById(R.id.TVQuestion);
        answerTextView = (TextView) v.findViewById(R.id.TVAnswer);
        timeTextView = (TextView) v.findViewById(R.id.TVTime);
        optionsRadioGroup = (RadioGroup) v.findViewById(R.id.RGOptions);
        o1RadioButton = (RadioButton) v.findViewById(R.id.RBOption1);
        o2RadioButton = (RadioButton) v.findViewById(R.id.RBOption2);
        o3RadioButton = (RadioButton) v.findViewById(R.id.RBOption3);
        o4RadioButton = (RadioButton) v.findViewById(R.id.RBOption4);
        o5RadioButton = (RadioButton) v.findViewById(R.id.RBOption5);
        answerEditText = (EditText) v.findViewById(R.id.ETAnswer);
        nextButton = (Button) v.findViewById(R.id.BTNext);
        prevButton = (Button) v.findViewById(R.id.BTPrev);
        submitButton = (Button) v.findViewById(R.id.BTSubmit);
        if (((QuizActivity) getActivity()).hasSubmitted() == true){
            //Set text to finish
            submitButton.setText("Finish");
        }

        //Set Values
        questionTextView.setText(Html.fromHtml(q.getContent()));
        answerTextView.setText(Html.fromHtml(q.getAnswer()));
        if (((QuizActivity) getActivity()).hasSubmitted()) answerTextView.setVisibility(View.VISIBLE);
        else answerTextView.setVisibility(View.GONE);

        //If Not MCQ then make Edit Text visible assuming this is a written answered question
        if (q.getOption1().isEmpty() && q.getOption2().isEmpty() && q.getOption3().isEmpty())
            answerEditText.setVisibility(View.VISIBLE);
        else {
            //Set Text
            o1RadioButton.setText(Html.fromHtml(q.getOption1()));
            o2RadioButton.setText(Html.fromHtml(q.getOption2()));
            o3RadioButton.setText(Html.fromHtml(q.getOption3()));
            o4RadioButton.setText(Html.fromHtml(q.getOption4()));
            o5RadioButton.setText(Html.fromHtml(q.getOption5()));
            //Make Visible
            if(!q.getOption1().equals("-")) o1RadioButton.setVisibility(View.VISIBLE);
            if(!q.getOption2().equals("-")) o2RadioButton.setVisibility(View.VISIBLE);
            if(!q.getOption3().equals("-")) o3RadioButton.setVisibility(View.VISIBLE);
            if(!q.getOption4().equals("-")) o4RadioButton.setVisibility(View.VISIBLE);
            if(!q.getOption5().equals("-")) o5RadioButton.setVisibility(View.VISIBLE);
        }

        //Get Questions
        ArrayList<Question> questions = ((QuizActivity) getActivity()).getQuestions();
        //Index of current Question in quiz
        currentIndex = indexOf(questions,q);
        //Total number of Questions in quiz
        final int length = questions.size() - 1;
        //If current question is last in quiz
        if (currentIndex == length) {
            //Hide NEXT & PREV button and Show SUBMIT button
            nextButton.setVisibility(View.GONE);
            prevButton.setVisibility(View.GONE);
            submitButton.setVisibility(View.VISIBLE);
        }

        //When Next Button is clicked
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Record User Answer
                recordAnswer(optionsRadioGroup, answerEditText, currentIndex);
                //Move to next
                if (currentIndex != length) ((QuizActivity) getActivity()).setCurrentItem(currentIndex + 1, true);
            }
        });

        //When Prev Button is clicked
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Move to previous
                if (currentIndex != 0) ((QuizActivity) getActivity()).setCurrentItem(currentIndex - 1, true);
            }
        });

        //When Next Button is clicked
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowFinishQuizDialog();
            }
        });
        return v;
    }

    public void ShowFinishQuizDialog() {
        if (((QuizActivity) getActivity()).hasSubmitted() == true){
            //Take user back to Menu
            Intent intent = new Intent(getActivity(), Menu2Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            getActivity().finish();
        }else {
            //Show Dialog
            new AlertDialog.Builder(getActivity()).setTitle("Submit").setMessage("Are You Sure You Want To Submit Answers?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        //On confirmation
                        public void onClick(DialogInterface dialog, int which) {
                            FinishQuiz();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        //If User Says 'No', Dismiss!
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    public void FinishQuiz() {
        //Record User Answer
        recordAnswer(optionsRadioGroup, answerEditText, currentIndex);
        //Set Answer TextView's Visibility 'True'
        ((QuizActivity) getActivity()).setHasSubmitted(true);
        //Restart at first question but only to show answers
        ((QuizActivity) getActivity()).setCurrentItem(0, true);
        //Set text to finish
        submitButton.setText("Finish");
        //Calculate User Stats
        calculateResults(((QuizActivity) getActivity()).getQuestions(), ((QuizActivity) getActivity()).getAnswers());
    }

    private void recordAnswer(RadioGroup optionsRadioGroup, EditText answerEditText, int currentIndex) {

        String answer = "";
        int selectedRadioButtonID = optionsRadioGroup.getCheckedRadioButtonId();
        RadioButton selectedRadioButton = (RadioButton) optionsRadioGroup.findViewById(selectedRadioButtonID);


        //If both EditText and RadioButton are left empty, prompt about no answer
        if (answer.isEmpty() && selectedRadioButton == null) { Utils.showAlertDialogWithoutCancel(getActivity(), "No Answer Submitted", "Please Provide An Answer Before Proceeding!"); return; }

        //Record Answer from EditText or RadioButton
        if(answerEditText.getVisibility() == View.VISIBLE){ answer = answerEditText.getText().toString(); }
        else answer = selectedRadioButton.getText().toString();

        //Insert in answer array
        ((QuizActivity) getActivity()).addAnswerAtPosition(answer, currentIndex);
    }

    private int indexOf(ArrayList<Question> questions, Question question) {
        for (Question q : questions) {
            if (question.getId() == q.getId()) {
                return questions.indexOf(q);
            }
        }
        return 0;
    }

    private void calculateResults(ArrayList<Question> questions, ArrayList<String> answers) {
        /**Calculate*/
        int correctAnswers = 0;
        //Count correct answers
        for (int i = 0; i < questions.size(); i++) {
            if (answers.size() > i && questions.get(i).getAnswer().equals(answers.get(i))) {
                correctAnswers++;
            }
        }
        //Calculate percentage
        float percentage = (correctAnswers * 100.0f) / questions.size();
        Log.e(TAG,"Percent: "+percentage);
        /**Show dialog*/
        Utils.showAlertDialogWithoutCancel(getActivity(), "Result", "You Correctly Answered " + correctAnswers + " Out Of " + questions.size() + " Questions\nAccuracy: " + String.format("%.2f", percentage) + "%");
        //Get Username from prefs
        int userID = getActivity().getSharedPreferences("Id", Context.MODE_PRIVATE).getInt(LoginActivity.EXTRA_USER_ID, 0);
        //Get Quiz ID
        int quizID = ((QuizActivity) getActivity()).getQuizID();
        //Send Results to server
        sendResults(userID, quizID, questions.size(), correctAnswers, percentage);
    }

    private void sendResults(int userID, int quizID, int questions, int correctAnswers, float percentage) {
        String URL = "http://everythinghere.in/app/index.php/records/" + userID + "/" + quizID + "/" + questions + "/" + correctAnswers + "/" + percentage;

        if (userID <= 0) { Utils.showAlertDialogWithoutCancel(getActivity(), "Error", "No Or Invalid User ID"); return; }
        if (quizID <= 0) { Utils.showAlertDialogWithoutCancel(getActivity(), "Error", "No Or Invalid Quiz ID"); return; }
        if (questions <= 0) { Utils.showAlertDialogWithoutCancel(getActivity(), "Error", "No Or Invalid Number of Quizzes"); return; }

        if (Utils.isNetworkOnline(getActivity())) {
            ((QuizActivity) getActivity()).showProgress(true);
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            ((QuizActivity) getActivity()).showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            Toast.makeText(getActivity(), e.getMessage(),Toast.LENGTH_SHORT).show();

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            ((QuizActivity) getActivity()).showProgress(false);
                            Utils.showAlertDialogWithoutCancel(getActivity(), "Failed", "Error Occurred While Fetching Questions");
                        }
                    });
            ((QuizActivity) getActivity()).getQueue().add(request);
        } else {
            ((QuizActivity) getActivity()).showProgress(false);
            Utils.showAlertDialogWithoutCancel(getActivity(), "Error", "Network Not Found!");
        }
    }

    public void updateTime(long l){
        Date date = new Date(l);
        DateFormat formatter = new SimpleDateFormat("mm:ss");
        String dateFormatted = formatter.format(date);
        Log.e(TAG,dateFormatted);
        timeTextView.setText(dateFormatted);
    }
}
