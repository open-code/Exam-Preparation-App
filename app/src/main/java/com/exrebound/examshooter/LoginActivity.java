package com.exrebound.examshooter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooter.models.ErrorMessage;
import com.exrebound.examshooter.models.User;
import com.exrebound.examshooter.utils.Parser;
import com.exrebound.examshooter.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_USERNAME = "com.exrebound.examshooter.username";
    public static final String EXTRA_PASSWORD = "com.exrebound.examshooter.password";
    public static final String EXTRA_USER_ID = "com.exrebound.examshooter.id";
    public static final String EXTRA_IS_LOGIN = "com.exrebound.examshooter.is_login";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private View mProgressView;
    private EditText usernameEditText, passwordEditText, usernameRegisterEditText, passwordRegisterEditText, dobRegisterEditText, emailRegisterEditText;
    private Button loginButton,signUpButton;
    private ImageView logoImageView;
    private LinearLayout container, loginForm, signupForm;
    private TextView signupTextView,loginTextView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private RequestQueue queue;

    @Override
    protected void onStart() {
        super.onStart();

        //Save User Login Status to prefs
        boolean isLogin = getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean(EXTRA_IS_LOGIN, false);

        if(isLogin){
            //Save User ID to prefs
            int id = getSharedPreferences("Id", Context.MODE_PRIVATE).getInt(EXTRA_USER_ID, 0);
            //If no ID then force Re-Login
            if(id <= 0){
                Utils.showAlertDialogWithoutCancel(this,"Login Error",
                        "Invalid or No User ID Found, Please Login Again To Continue Using Application");
                return;
            }
            //Else Proceed to main menu
            Intent i = new Intent(LoginActivity.this, Menu2Activity.class);
            i.putExtra(EXTRA_USER_ID,id);
            startActivity(i);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        container = (LinearLayout) findViewById(R.id.LLContainer);
        loginForm = (LinearLayout) findViewById(R.id.LLLoginForm);
        signupForm = (LinearLayout) findViewById(R.id.LLRegisterForm);
        mProgressView = findViewById(R.id.login_progress);
        usernameEditText = (EditText) findViewById(R.id.ETUsername);
        passwordEditText = (EditText) findViewById(R.id.ETPassword);
        usernameRegisterEditText = (EditText) findViewById(R.id.ETRUsername);
        passwordRegisterEditText= (EditText) findViewById(R.id.ETRPassword);
        dobRegisterEditText= (EditText) findViewById(R.id.ETRDOB);
        dobRegisterEditText.setInputType(InputType.TYPE_NULL);
        dobRegisterEditText.requestFocus();
        emailRegisterEditText= (EditText) findViewById(R.id.ETREmail);
        loginButton = (Button) findViewById(R.id.BLogIn);
        signUpButton = (Button) findViewById(R.id.BTRSingUP);
        signupTextView = (TextView) findViewById(R.id.TVSignUp);
        signupTextView.setText(Utils.getMultiStyleSignUpText("Don't Have Account Yet?\nSign Up Now!", Color.WHITE, 23, 31, true, true));
        loginTextView = (TextView) findViewById(R.id.TVLogin);
        loginTextView.setText(Utils.getMultiStyleSignUpText("Already Have An Account?\nLog In Now!",Color.WHITE,25,31,true,true));
        //Load Username from prefs
        String username = getSharedPreferences("Username", Context.MODE_PRIVATE).getString(EXTRA_USERNAME, "");
        usernameEditText.setText(username);
        // On Click listeners
        usernameEditText.setOnClickListener(this);
        passwordEditText.setOnClickListener(this);
        usernameRegisterEditText.setOnClickListener(this);
        passwordRegisterEditText.setOnClickListener(this);
        dobRegisterEditText.setOnClickListener(this);
        emailRegisterEditText.setOnClickListener(this);
        signupTextView.setOnClickListener(this);
        loginTextView.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        logoImageView = (ImageView) findViewById(R.id.IVLogo);
        //TODO To be remove after debugging
        //logoImageView.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { passwordEditText.setText("admin");    } });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin(final String username, final String password) {
        String URL = "http://everythinghere.in/app/index.php/login/" + username + "/" + password;
        if (Utils.isNetworkOnline(this)) {
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            User u = Parser.getUserLoginResponse(s);
                            showProgress(false);
                            if (u.getUsername() != null) {
                                Toast.makeText(LoginActivity.this, "Welcome. " + u.getUsername() + "!", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(LoginActivity.this, Menu2Activity.class);
                                String oldname = getSharedPreferences("Username", Context.MODE_PRIVATE).getString(EXTRA_USERNAME, "");

                                //If username is different from prefs' username
                                if (!u.getUsername().equals(oldname)) {
                                    //Save Username to prefs
                                    getSharedPreferences("Username", Context.MODE_PRIVATE).edit().putString(EXTRA_USERNAME, u.getUsername()).commit();
                                }
                                if(u.getId() >= 0){
                                    //Save User ID to prefs
                                    getSharedPreferences("Id", Context.MODE_PRIVATE).edit().putInt(EXTRA_USER_ID, u.getId()).commit();
                                }

                                //Save Password to prefs
                                getSharedPreferences("Password", Context.MODE_PRIVATE).edit().putString(EXTRA_PASSWORD, password).commit();
                                queue.cancelAll(new RequestQueue.RequestFilter() {
                                    @Override
                                    public boolean apply(Request<?> request) {
                                        return true;
                                    }
                                });

                                //Save User Login Status to prefs
                                getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(EXTRA_IS_LOGIN, true).commit();

                                i.putExtra(EXTRA_USER_ID,u.getId());
                                startActivity(i);
                                finish();
                            } else {
                                ErrorMessage e = Parser.getError(s);
                                if (e != null)
                                    Utils.showLightSnackbar(container, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(container, "Error Occurred While Logging In...");
                        }
                    });
            queue = Volley.newRequestQueue(this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(container, "Network Not Found!");
        }
    }

    /**
     * Attempts to sign up or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptRegister(final String username, final String email, final String password, final String dob) {
        String URL = "http://everythinghere.in/app/index.php/register/" + username + "/" + email + "/" + password + "/" + dob;
        if (Utils.isNetworkOnline(this)) {
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            //Switch Forms and Hide Sign Up Text
                            signupTextView.setVisibility(View.VISIBLE);
                            loginForm.setVisibility(View.VISIBLE);
                            signupForm.setVisibility(View.GONE);
                            ErrorMessage e = Parser.getError(s);
                            if (e != null) {
                                if (e.getSuccess() == 1)
                                    showLoginForm();
                                Utils.showLightSnackbar(container, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(container, "Error Occurred While Logging In...");
                        }
                    });
            queue = Volley.newRequestQueue(this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(container, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BLogIn) {
            showProgress(true);
            String username = usernameEditText.getText().toString();
            String password = passwordEditText.getText().toString();
            if (username.isEmpty()) {
                usernameEditText.setError("Field Can't Be Left Empty");
                return;
            }
            if (password.isEmpty()) {
                passwordEditText.setError("Field Can't Be Left Empty");
                return;
            }
            attemptLogin(username, password);
        } else if (id == R.id.BTRSingUP) {
            showProgress(true);
            String username = usernameRegisterEditText.getText().toString();
            String password = passwordRegisterEditText.getText().toString();
            String dob = dobRegisterEditText.getText().toString();
            String email = emailRegisterEditText.getText().toString();
            if (username.isEmpty()) { usernameRegisterEditText.setError("Field Can't Be Left Empty"); return; }
            if (password.isEmpty()) { passwordRegisterEditText.setError("Field Can't Be Left Empty"); return; }
            if (dob.isEmpty()) { dobRegisterEditText.setError("Field Can't Be Left Empty"); return; }
            if (email.isEmpty()) { emailRegisterEditText.setError("Field Can't Be Left Empty"); return; }
            attemptRegister(username, email, password, dob);
        } else if (id == R.id.ETUsername || id == R.id.ETPassword || id == R.id.ETREmail || id == R.id.ETRPassword || id == R.id.ETRUsername) {
            showProgress(false);
        } else if (id == R.id.TVSignUp) {
            //Switch Forms and Hide Sign Up Text
            signupTextView.setVisibility(View.GONE);
            loginTextView.setVisibility(View.VISIBLE);
            loginForm.setVisibility(View.GONE);
            signupForm.setVisibility(View.VISIBLE);
        } else if (id == R.id.TVLogin) {
            showLoginForm();
        } else if(id == R.id.ETRDOB){
            Calendar newCalendar = Calendar.getInstance();
            DatePickerDialog d = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                    dobRegisterEditText.setText(dateFormatter.format(newDate.getTime()));
                }
            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            d.show();
        }
    }

    private void showLoginForm() {
        //Switch Forms and Hide Sign Up Text
        signupTextView.setVisibility(View.VISIBLE);
        loginTextView.setVisibility(View.GONE);
        loginForm.setVisibility(View.VISIBLE);
        signupForm.setVisibility(View.GONE);
    }
}
