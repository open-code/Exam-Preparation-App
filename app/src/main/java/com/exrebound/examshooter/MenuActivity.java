package com.exrebound.examshooter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.exrebound.examshooter.utils.Utils;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_ID = "com.exrebound.examshooter.id";
    public static final String EXTRA_LEADERBOARD = "com.exrebound.examshooter.leaderboard";
    private static final String TAG = ".menu_activity";
    private static final String EXTRA_USER_ID = "user_id";
    LinearLayout quizLayout, postsLayout, recordsLayout,leaderboardLayout;
    private int userID;
    private boolean isLogin;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userID = getIntent().getIntExtra(LoginActivity.EXTRA_USER_ID,0);
        //Save User ID to prefs
        getSharedPreferences("user_id", Context.MODE_PRIVATE).edit().putInt(EXTRA_USER_ID, userID).commit();

        //Save User Login Status to prefs
        isLogin = getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean(LoginActivity.EXTRA_IS_LOGIN, false);

        quizLayout = (LinearLayout) findViewById(R.id.IBQuizzes);
        postsLayout = (LinearLayout) findViewById(R.id.IBPosts);
        recordsLayout = (LinearLayout) findViewById(R.id.IBRecords);
        leaderboardLayout = (LinearLayout) findViewById(R.id.IBLeaderboard);
        quizLayout.setOnClickListener(this);
        postsLayout.setOnClickListener(this);
        recordsLayout.setOnClickListener(this);
        leaderboardLayout.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        //Save User Login Status to prefs
        boolean isLogin = getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean(LoginActivity.EXTRA_IS_LOGIN, false);
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        //If login, show logout
        if(isLogin) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }
        //If logged out, show login
        else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.MILogOut) {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();
        }
        if(id == R.id.MILogIn){
            //Go to login activity
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();

            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.IBQuizzes){
            if(!isLogin){
                Utils.showAlertDialogWithoutCancel(MenuActivity.this,"Sign In","Please Sign In To Access This Feature");
                return;
            }
            startActivity(new Intent(this,MainActivity.class));
        }else if(id == R.id.IBPosts){
            startActivity(new Intent(this,CategoriesActivity.class));
        }else if(id == R.id.IBRecords){
            if(!isLogin){
                Utils.showAlertDialogWithoutCancel(MenuActivity.this,"Sign In","Please Sign In To Access This Feature");
                return;
            }
            Intent i = new Intent(this, RecordsActivity.class);
            i.putExtra(EXTRA_LEADERBOARD, false);
            int userID = getSharedPreferences("Id", Context.MODE_PRIVATE).getInt(EXTRA_USER_ID, 0);
            i.putExtra(EXTRA_ID, userID);
            startActivity(i);
        }else if(id == R.id.IBLeaderboard){
            if(!isLogin){
                Utils.showAlertDialogWithoutCancel(MenuActivity.this,"Sign In","Please Sign In To Access This Feature");
                return;
            }
            Intent i = new Intent(this, RecordsActivity.class);
            i.putExtra(EXTRA_LEADERBOARD,true);
            startActivity(i);
        }
    }
}
