package com.exrebound.examshooter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooter.models.ErrorMessage;
import com.exrebound.examshooter.models.Quiz;
import com.exrebound.examshooter.utils.Parser;
import com.exrebound.examshooter.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_QUIZ_ID = ".quiz_id";
    public static final String EXTRA_QUIZ_NAME = ".quiz_name";
    public static final String EXTRA_QUIZ_DURATION = ".quiz_duration";
    private static final String TAG = ".MainActivity";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private ListView quizzesListView;
    private TextView emptyTextView,usernameTextView,questionsTextView,correctTextView,percentageTextView;
    private LinearLayout containerLinearLayout;
    private ProgressBar mProgressView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private RequestQueue queue;
    private QuizAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prepareUI();
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        quizzesListView = (ListView) findViewById(R.id.LVQuizes);
        emptyTextView = (TextView) findViewById(R.id.empty);
        quizzesListView.setEmptyView(emptyTextView);
        getQuizzes();
        quizzesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Quiz q = (Quiz) quizzesListView.getItemAtPosition(position);
                if(getDifference(q).equalsIgnoreCase("expired")){
                    Utils.showAlertDialogWithoutCancel(MainActivity.this,"Quiz Expired","The Quiz You Are Trying To Attempt Has Already Been Expired");
                }else {
                    Intent i = new Intent(MainActivity.this, QuizActivity.class);
                    i.putExtra(EXTRA_QUIZ_ID, q.getId());
                    i.putExtra(EXTRA_QUIZ_DURATION, q.getDuration());
                    i.putExtra(EXTRA_QUIZ_NAME, q.getTitle());
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Save User Login Status to prefs
        boolean isLogin = getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean(LoginActivity.EXTRA_IS_LOGIN, false);
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        //If login, show logout
        if(isLogin) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }
        //If logged out, show login
        else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.MILogOut) {
            Intent intent = new Intent(this, LoginActivity.class);

            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        if(id == R.id.MILogIn){
            //Go to login activity
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();

            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getQuizzes() {
        String URL = "http://everythinghere.in/app/index.php/quiz";
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ArrayList<Quiz> quizzes = Parser.getQuizzes(s);
                            showProgress(false);
                            if (quizzes != null && quizzes.size() > 0) {
                                adapter = new QuizAdapter(MainActivity.this, R.layout.records_list_item, quizzes);
                                quizzesListView.setAdapter(adapter);
                            } else {
                                ErrorMessage e = Parser.getError(s);
                                if (e != null)
                                    Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Logging In...");
                        }
                    });
            queue = Volley.newRequestQueue(MainActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private class QuizAdapter extends ArrayAdapter<Quiz>{
        public QuizAdapter(Context context, int resource, List<Quiz> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getLayoutInflater().inflate(R.layout.records_list_item, null);

            Quiz q = getItem(position);

            usernameTextView = (TextView) convertView.findViewById(R.id.TVUsername);
            questionsTextView = (TextView) convertView.findViewById(R.id.TVQuestions);
            correctTextView = (TextView) convertView.findViewById(R.id.TVCorrect);
            percentageTextView = (TextView) convertView.findViewById(R.id.TVPercentage);
            usernameTextView.setText(q.getTitle());
            questionsTextView.setText("Questions: " + q.getQuestions());
            correctTextView.setText("Duration: " + q.getDuration() +" Min");
            percentageTextView.setText("Ends In: " + getDifference(q));

            return convertView;
        }
    }

    //Add Ends In Functionality
    private String getDifference(Quiz q) {
        String endsIn = "";
        percentageTextView.setTextColor(Color.BLACK);

        //IF DATE ADDED + ENDS IN < TODAY;
        try {
            //Date Added
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date dateAdded = format.parse(q.getDate_added());

            //Expiry Date
            Calendar expiryDate = Calendar.getInstance();
            expiryDate.setTime(dateAdded);
            expiryDate.add(Calendar.DATE, q.getEndsIn());

            //Today's Date
            Calendar today = Calendar.getInstance();

            if(expiryDate.after(today)){
                //Days = Expiry Date - Today
                long diff = expiryDate.getTime().getTime() - today.getTime().getTime();
                endsIn = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+" Days";
                if(endsIn.equalsIgnoreCase("0 Days")){
                    endsIn = "Today";
                }
            } else{
                percentageTextView.setTextColor(Color.RED);
                endsIn = "Expired";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return endsIn;
    }
}
