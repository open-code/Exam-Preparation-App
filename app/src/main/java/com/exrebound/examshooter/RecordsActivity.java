package com.exrebound.examshooter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooter.models.ErrorMessage;
import com.exrebound.examshooter.models.Record;
import com.exrebound.examshooter.utils.Parser;
import com.exrebound.examshooter.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RecordsActivity extends AppCompatActivity {

    private static final String TAG = ".RecordsActivity";
    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private ListView categoriesListView;
    private TextView emptyTextView,usernameTextView,questionsTextView,correctTextView,percentageTextView;
    private LinearLayout containerLinearLayout;
    private ProgressBar mProgressView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private RequestQueue queue;
    private RecordsAdapter adapter;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    private boolean isLeaderboard = false;
    private String username;
    private int userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        isLeaderboard = getIntent().getExtras().getBoolean(Menu2Activity.EXTRA_LEADERBOARD, false);
        userID = getSharedPreferences("Id", Context.MODE_PRIVATE).getInt(LoginActivity.EXTRA_USER_ID, 0);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prepareUI();
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        categoriesListView = (ListView) findViewById(R.id.LVRecords);
        emptyTextView = (TextView) findViewById(R.id.empty);
        emptyTextView.setText("No Items In List");
        categoriesListView.setEmptyView(emptyTextView);
        if (isLeaderboard) getLeaderboard();
        else getRecords(userID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Save User Login Status to prefs
        boolean isLogin = getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean(LoginActivity.EXTRA_IS_LOGIN, false);
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        //If login, show logout
        if(isLogin) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }
        //If logged out, show login
        else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.MILogOut) {
            //Records are not allowed for unregistered users, take to the categories
            Intent intent = new Intent(this, CategoriesActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();

            startActivity(intent);
            finish();
        }
        if(id == R.id.MILogIn){
            //Records are not allowed for unregistered users, take to the categories
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();

            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getRecords(int userID) {
        if (userID != 0) {

            String URL = "http://everythinghere.in/app/index.php/records/" + userID;
            if (Utils.isNetworkOnline(this)) {
                showProgress(true);
                StringRequest request = new StringRequest(Request.Method.GET, URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                showProgress(false);
                                ArrayList<Record> categories = Parser.getRecords(s);
                                if (categories != null && categories.size() > 0) {
                                    adapter = new RecordsAdapter(RecordsActivity.this, R.layout.records_list_item, categories);
                                    categoriesListView.setAdapter(adapter);
                                } else {
                                    ErrorMessage e = Parser.getError(s);
                                    if (e != null) Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                showProgress(false);
                                Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                            }
                        });
                queue = Volley.newRequestQueue(RecordsActivity.this);
                queue.add(request);
            } else {
                showProgress(false);
                Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
            }
        } else Utils.showAlertDialogWithoutCancel(this, "User Not Found", "Invalid or No User ID Found.");

    }

    public void getLeaderboard() {
        String URL = "http://everythinghere.in/app/index.php/records";
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ArrayList<Record> records = Parser.getRecords(s);
                            if (records != null && records.size() > 0) {
                                adapter = new RecordsAdapter(RecordsActivity.this, R.layout.records_list_item, records);
                                categoriesListView.setAdapter(adapter);
                            } else {
                                ErrorMessage e = Parser.getError(s);
                                if (e != null) Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            queue = Volley.newRequestQueue(RecordsActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {
        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private class RecordsAdapter extends ArrayAdapter<Record>{
        public RecordsAdapter(Context context, int resource, List<Record> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getLayoutInflater().inflate(R.layout.records_list_item, null);

            Record r = getItem(position);

            usernameTextView = (TextView) convertView.findViewById(R.id.TVUsername);
            questionsTextView = (TextView) convertView.findViewById(R.id.TVQuestions);
            correctTextView = (TextView) convertView.findViewById(R.id.TVCorrect);
            percentageTextView = (TextView) convertView.findViewById(R.id.TVPercentage);
            setUsername(r, userID);
            questionsTextView.setText("Questions: " + r.getQuestions());
            correctTextView.setText("Correct: " + r.getCorrect());
            percentageTextView.setText("%: " + r.getPercentage());

            return convertView;
        }
    }

    private void setUsername(Record r, int userID) {
        //Fetch username if record's ID is not equal to current user's id, else use current user's name
        if(r.getUserId() == userID) usernameTextView.setText(getSharedPreferences("Username", Context.MODE_PRIVATE).getString(LoginActivity.EXTRA_USERNAME,""));
        else getUsername(r.getUserId());
    }

    private void getUsername(int id) {
        Log.e(TAG,"Param ID: "+id);
        String URL = "http://everythinghere.in/app/index.php/user/"+id;
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            try {
                                JSONObject object = new JSONObject(s);
                                Log.e(TAG,object.toString());
                                if(object.getInt("success") == 1) {
                                    username = object.getString("username");
                                    usernameTextView.setText(username);
                                }else{
                                    usernameTextView.setText("User No Longer A Member");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching User Information");
                        }
                    });
            if(queue == null) queue = Volley.newRequestQueue(RecordsActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }
}
