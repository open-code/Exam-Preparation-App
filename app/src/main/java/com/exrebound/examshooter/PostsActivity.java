package com.exrebound.examshooter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooter.models.ErrorMessage;
import com.exrebound.examshooter.utils.Parser;
import com.exrebound.examshooter.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.listviewfeed.data.Post;

public class PostsActivity extends AppCompatActivity {

    public static final String EXTRA_CONTENT = ".content";
    public static final String EXTRA_TITLE = ".title";
    public static final String EXTRA_DATE = ".date";
    public static final String EXTRA_ID = ".id";
    public static final String EXTRA_IMAGE = ".image";
    public static final String EXTRA_IMAGE_STRING = ".image_string";
    private static final String TAG = ".PostsActivity";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private ListView postsListView;
    private TextView emptyTextView;
    private LinearLayout containerLinearLayout;
    private ProgressBar mProgressView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private RequestQueue queue;
    private FeedListAdapter adapter;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    private ArrayList<Post> posts;
    private int categoryID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        categoryID = getIntent().getIntExtra(CategoriesActivity.EXTRA_CATEGORY_ID,0);

        //If no id, then load from prefs
        if(categoryID <= 0)
            categoryID = getSharedPreferences("category_id", Context.MODE_PRIVATE).getInt("id", 0);
        else
            getSharedPreferences("category_id",Context.MODE_PRIVATE).edit().putInt("id",categoryID).commit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prepareUI();
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        postsListView = (ListView) findViewById(R.id.LVPosts);
        postsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(queue != null) queue.cancelAll(new RequestQueue.RequestFilter() {
                    @Override
                    public boolean apply(Request<?> request) {
                        return true;
                    }
                });
                Post p = (Post) postsListView.getItemAtPosition(position);
                Intent i = new Intent(PostsActivity.this, PostDetailActivity.class);
                i.putExtra(EXTRA_CONTENT,p.getContent());
                i.putExtra(EXTRA_TITLE,p.getTitle());
                i.putExtra(EXTRA_DATE,p.getDate());
                i.putExtra(EXTRA_ID,p.getId());
                i.putExtra(EXTRA_IMAGE,p.getImage());
                i.putExtra(EXTRA_IMAGE_STRING, p.getImageString());
                startActivity(i);
            }
        });
        emptyTextView = (TextView) findViewById(R.id.empty);
        postsListView.setEmptyView(emptyTextView);
        getPosts();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Save User Login Status to prefs
        boolean isLogin = getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean(LoginActivity.EXTRA_IS_LOGIN, false);
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        //If login, show logout
        if(isLogin) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }
        //If logged out, show login
        else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.MILogOut) {
            //Posts are allowed for unregistered users, take to the categories
            Intent intent = new Intent(this, CategoriesActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();

            startActivity(intent);
            finish();
        }
        if(id == R.id.MILogIn){
            //Records are not allowed for unregistered users, take to the categories
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            //Save User Login Status to prefs
            getSharedPreferences("login", Context.MODE_PRIVATE).edit().putBoolean(LoginActivity.EXTRA_IS_LOGIN, false).commit();

            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getPosts() {
        String URL = "http://everythinghere.in/app/index.php/post/category/"+categoryID;
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            posts = Parser.getPosts(s);
                            if (posts != null && posts.size() > 0) {
                                adapter = new FeedListAdapter(PostsActivity.this, posts);
                                postsListView.setAdapter(adapter);
                            } else {
                                ErrorMessage e = Parser.getArrayError(s);
                                if (e != null) Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            if(queue == null)
                queue = Volley.newRequestQueue(PostsActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public class FeedListAdapter extends BaseAdapter {
        private static final String IMAGE_URL = "http://everythinghere.in/app/images/";
        private Activity activity;
        private LayoutInflater inflater;
        private List<Post> posts;
        Bitmap image;

        public FeedListAdapter(Activity activity, List<Post> posts) {
            this.activity = activity;
            this.posts = posts;
        }

        @Override
        public int getCount() {
            return posts.size();
        }

        @Override
        public Object getItem(int location) {
            return posts.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (inflater == null)
                inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null)
                convertView = inflater.inflate(R.layout.feed_item2, null);

            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView timestamp = (TextView) convertView
                    .findViewById(R.id.timestamp);
            TextView statusMsg = (TextView) convertView
                    .findViewById(R.id.txtStatusMsg);
            ImageView feedImageView = (ImageView) convertView
                    .findViewById(R.id.feedImage1);

            Post item = posts.get(position);

            name.setText(item.getTitle());

            timestamp.setText(item.getDate());

            // Check for empty status message
            if (!TextUtils.isEmpty(item.getContent())) {
                statusMsg.setMaxLines(3);
                statusMsg.setEllipsize(TextUtils.TruncateAt.END);
                statusMsg.setText(item.getContent());
                statusMsg.setVisibility(View.VISIBLE);
            } else {
                // status is empty, remove from view
                statusMsg.setVisibility(View.GONE);
            }

            /*
            // Feed image
                if (item.getImage() != null && item.getImage().length() > 0 && !item.getImage().equals("-")) {
                    getImage(IMAGE_URL + item.getImage(), feedImageView);
                } else {
                    feedImageView.setVisibility(View.GONE);
                }
            */
            return convertView;
        }

        private void getImage(String url, final ImageView feedImageView) {
            url = url.replace(" ", "%20");
            if (Utils.isNetworkOnline(PostsActivity.this)) {
                showProgress(true);
                ImageRequest request = new ImageRequest(url, new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        showProgress(false);
                        feedImageView.setImageBitmap(bitmap);
                        feedImageView.setVisibility(View.VISIBLE);
                    }
                }, 0, 0, null, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        showProgress(false);
                        Utils.showAlertDialog(PostsActivity.this, "Error", "Error Occurred while Downloading Image..!");
                    }
                });
                if(queue == null)
                    queue = Volley.newRequestQueue(PostsActivity.this);
                queue.add(request);
            } else {
                Utils.showAlertDialog(PostsActivity.this,"Error", "Network Not Found!");
            }
        }
    }
}
