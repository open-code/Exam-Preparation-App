Exam preparation app is the client side app for student exam preparation system.

Key features include:
- User Registration/Sign in
- Add/Edit Quiz
    - Add MCQ
    - Add Short Answered Questions
- Admin Posts
- User Records
- Quiz Catagories
- Quiz Attempt